
/*===========================================================================*\
*                                                                            *
*                              OpenFlipper                                   *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
*                                                                            *
\*===========================================================================*/

#include <pybind11/pybind11.h>
#include <pybind11/embed.h>


#include <MeshObjectInfoPlugin.hh>
#include <QString>
#include <QChar>

#include <OpenFlipper/BasePlugin/PythonFunctions.hh>
#include <OpenFlipper/PythonInterpreter/PythonTypeConversions.hh>

namespace py = pybind11;



PYBIND11_EMBEDDED_MODULE(InfoMeshObject, m) {

  QObject* pluginPointer = getPluginPointer("InfoMeshObject");

  if (!pluginPointer) {
     std::cerr << "Error Getting plugin pointer for Plugin-InfoMeshObject" << std::endl;
     return;
   }

  InfoMeshObjectPlugin* plugin = qobject_cast<InfoMeshObjectPlugin*>(pluginPointer);

  if (!plugin) {
    std::cerr << "Error converting plugin pointer for Plugin-InfoMeshObject" << std::endl;
    return;
  }

  // Export our core. Make sure that the c++ worlds core object is not deleted if
  // the python side gets deleted!!
  py::class_< InfoMeshObjectPlugin,std::unique_ptr<InfoMeshObjectPlugin, py::nodelete> > info(m, "InfoMeshObject");

  // On the c++ side we will just return the existing core instance
  // and prevent the system to recreate a new core as we need
  // to work on the existing one.
  info.def(py::init([plugin]() { return plugin; }));


  info.def("vertexCount", &InfoMeshObjectPlugin::vertexCount,
                                 QCoreApplication::translate("PythonDocInfoMeshObject","get total number of verticies of a given object").toLatin1().data(),
                         py::arg(QCoreApplication::translate("PythonDocInfoMeshObject","ID of the object").toLatin1().data()));

  info.def("edgeCount", &InfoMeshObjectPlugin::edgeCount,
                                 QCoreApplication::translate("PythonDocInfoMeshObject","get total number of edges of a given object").toLatin1().data(),
                         py::arg(QCoreApplication::translate("PythonDocInfoMeshObject","ID of the object").toLatin1().data()));

  info.def("faceCount", &InfoMeshObjectPlugin::faceCount,
                                 QCoreApplication::translate("PythonDocInfoMeshObject","get total number of faces of a given object").toLatin1().data(),
                         py::arg(QCoreApplication::translate("PythonDocInfoMeshObject","ID of the object").toLatin1().data()));

  info.def("boundaryCount", &InfoMeshObjectPlugin::boundaryCount,
                                 QCoreApplication::translate("PythonDocInfoMeshObject","get total number of boundaries of a given object").toLatin1().data(),
                         py::arg(QCoreApplication::translate("PythonDocInfoMeshObject","ID of the object").toLatin1().data()));

  info.def("componentCount", &InfoMeshObjectPlugin::componentCount,
                                 QCoreApplication::translate("PythonDocInfoMeshObject","get total number of components of a given object").toLatin1().data(),
                         py::arg(QCoreApplication::translate("PythonDocInfoMeshObject","ID of the object").toLatin1().data()));

  info.def("genus", &InfoMeshObjectPlugin::genus,
                                  QCoreApplication::translate("PythonDocInfoMeshObject","get the genus of a given object").toLatin1().data(),
                          py::arg(QCoreApplication::translate("PythonDocInfoMeshObject","ID of the object").toLatin1().data()));

  info.def("cog", &InfoMeshObjectPlugin::cog,
                                  QCoreApplication::translate("PythonDocInfoMeshObject","get the  center of gravity of a given object").toLatin1().data(),
                          py::arg(QCoreApplication::translate("PythonDocInfoMeshObject","ID of the object").toLatin1().data()));

  info.def("boundingBoxMin", &InfoMeshObjectPlugin::boundingBoxMin,
                                  QCoreApplication::translate("PythonDocInfoMeshObject","get minimum point of the axis-aligned bounding box").toLatin1().data(),
                          py::arg(QCoreApplication::translate("PythonDocInfoMeshObject","ID of the object").toLatin1().data()));

  info.def("boundingBoxMax", &InfoMeshObjectPlugin::boundingBoxMax,
                                  QCoreApplication::translate("PythonDocInfoMeshObject","get maximum point of the axis-aligned bounding box").toLatin1().data(),
                          py::arg(QCoreApplication::translate("PythonDocInfoMeshObject","ID of the object").toLatin1().data()));

  info.def("boundingBoxSize", &InfoMeshObjectPlugin::boundingBoxSize,
                                  QCoreApplication::translate("PythonDocInfoMeshObject","get the size of the axis-aligned bounding box").toLatin1().data(),
                          py::arg(QCoreApplication::translate("PythonDocInfoMeshObject","ID of the object").toLatin1().data()));


  info.def("edgeLength", &InfoMeshObjectPlugin::edgeLength,
                                  QCoreApplication::translate("PythonDocInfoMeshObject","Get the length of an edge").toLatin1().data(),
                          py::arg(QCoreApplication::translate("PythonDocInfoMeshObject","ID of the object").toLatin1().data()),
                          py::arg(QCoreApplication::translate("PythonDocInfoMeshObject","ID of the edge").toLatin1().data()));

  info.def("faceArea", &InfoMeshObjectPlugin::faceArea,
                                  QCoreApplication::translate("PythonDocInfoMeshObject","Get the area of a face").toLatin1().data(),
                          py::arg(QCoreApplication::translate("PythonDocInfoMeshObject","ID of the object").toLatin1().data()),
                          py::arg(QCoreApplication::translate("PythonDocInfoMeshObject","ID of the face").toLatin1().data()));

  info.def("aspectRatio", &InfoMeshObjectPlugin::aspectRatio,
                                  QCoreApplication::translate("PythonDocInfoMeshObject","Get the aspect ratio of a face").toLatin1().data(),
                          py::arg(QCoreApplication::translate("PythonDocInfoMeshObject","ID of the object").toLatin1().data()),
                          py::arg(QCoreApplication::translate("PythonDocInfoMeshObject","ID of the face").toLatin1().data()));

  info.def("vertexValence", &InfoMeshObjectPlugin::vertexValence,
                                  QCoreApplication::translate("PythonDocInfoMeshObject","Get the valence of a vertex").toLatin1().data(),
                          py::arg(QCoreApplication::translate("PythonDocInfoMeshObject","ID of the object").toLatin1().data()),
                          py::arg(QCoreApplication::translate("PythonDocInfoMeshObject","ID of the vertex").toLatin1().data()));

  info.def("minEdgeLength", &InfoMeshObjectPlugin::minEdgeLength,
                                  QCoreApplication::translate("PythonDocInfoMeshObject","Get the minimal edge length of an object").toLatin1().data(),
                          py::arg(QCoreApplication::translate("PythonDocInfoMeshObject","ID of the object").toLatin1().data()));

  info.def("maxEdgeLength", &InfoMeshObjectPlugin::maxEdgeLength,
                                  QCoreApplication::translate("PythonDocInfoMeshObject","Get the maximal edge length of an object").toLatin1().data(),
                          py::arg(QCoreApplication::translate("PythonDocInfoMeshObject","ID of the object").toLatin1().data()));

  info.def("meanEdgeLength", &InfoMeshObjectPlugin::meanEdgeLength,
                                  QCoreApplication::translate("PythonDocInfoMeshObject","Get the mean edge length of an object").toLatin1().data(),
                          py::arg(QCoreApplication::translate("PythonDocInfoMeshObject","ID of the object").toLatin1().data()));
}

