/*===========================================================================*\
*                                                                            *
*                              OpenFlipper                                   *
 *           Copyright (c) 2001-2015, RWTH-Aachen University                 *
 *           Department of Computer Graphics and Multimedia                  *
 *                          All rights reserved.                             *
 *                            www.openflipper.org                            *
 *                                                                           *
 *---------------------------------------------------------------------------*
 * This file is part of OpenFlipper.                                         *
 *---------------------------------------------------------------------------*
 *                                                                           *
 * Redistribution and use in source and binary forms, with or without        *
 * modification, are permitted provided that the following conditions        *
 * are met:                                                                  *
 *                                                                           *
 * 1. Redistributions of source code must retain the above copyright notice, *
 *    this list of conditions and the following disclaimer.                  *
 *                                                                           *
 * 2. Redistributions in binary form must reproduce the above copyright      *
 *    notice, this list of conditions and the following disclaimer in the    *
 *    documentation and/or other materials provided with the distribution.   *
 *                                                                           *
 * 3. Neither the name of the copyright holder nor the names of its          *
 *    contributors may be used to endorse or promote products derived from   *
 *    this software without specific prior written permission.               *
 *                                                                           *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS       *
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED *
 * TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A           *
 * PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER *
 * OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,  *
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,       *
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR        *
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF    *
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING      *
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS        *
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.              *
*                                                                            *
\*===========================================================================*/



//=============================================================================
//
//  CLASS InfoMeshObjectPlugin - IMPLEMENTATION
//
//=============================================================================


//== INCLUDES =================================================================


#include "MeshObjectInfoPlugin.hh"

#include <MeshTools/MeshInfoT.hh>

#include <Math_Tools/Math_Tools.hh>

#include "ValenceHistogramDialog.hh"


//== IMPLEMENTATION ==========================================================


InfoMeshObjectPlugin::InfoMeshObjectPlugin() :
        info_(0),
        infoBar_(0),
        lastPickedObject_(0),
        lastPickedObjectId_(-1)
{
}

InfoMeshObjectPlugin::~InfoMeshObjectPlugin() {

  //Info bar and dialog will be deleted by core widget
}


void InfoMeshObjectPlugin::initializePlugin() {

}

/// initialize the plugin
void InfoMeshObjectPlugin::pluginsInitialized() {

  //set the slot descriptions
  setDescriptions();

  if ( OpenFlipper::Options::gui()) {

    // Create info bar
    infoBar_ = new InfoBar();

    // Create info dialog
    info_ = new InfoDialog();

    connect(info_->valenceHistograms_pb, SIGNAL( clicked() ),
            this, SLOT( slotShowHistogram() ));

    // Set default pick mode in dialog box
    info_->pickMode->setCurrentIndex(0); // PICK_FACES

    emit addWidgetToStatusbar(infoBar_);
    infoBar_->hideCounts();
  }
  
}

//-----------------------------------------------------------------------------

DataType InfoMeshObjectPlugin::supportedDataTypes() {
    return DataType(DATA_POLY_MESH | DATA_TRIANGLE_MESH);
}

//-----------------------------------------------------------------------------

template< class MeshT >
void InfoMeshObjectPlugin::printMeshInfo( MeshT* _mesh , int _id, unsigned int _index, ACG::Vec3d& _hitPoint ) {

  bool face   = false;
  bool edge   = false;
  bool vertex = false;

  int closestVertexIndex = -1;
  int closestEdgeIndex   = -1;

  switch (info_->pickMode->currentIndex() ) {
    case 0 : //Face
      closestVertexIndex = getClosestVertexInFace(_mesh, _index, _hitPoint);
      closestEdgeIndex   = getClosestEdgeInFace  (_mesh, _index, _hitPoint);
      face = true;
      break;
    case 1 : //Edge
      closestVertexIndex = getClosestVertexFromEdge(_mesh, _index, _hitPoint);
      closestEdgeIndex   = _index;
      edge = true;
      break;
    case 2 : //Vertex
      closestVertexIndex = _index;
      vertex = true;
      break;
    default:
      emit log(LOGERR,"Error: unknown picking mode in printMeshInfo");
      return;
  }

  QLocale locale;

  QString name;

  // name
  BaseObject* obj = 0;
  if ( PluginFunctions::getObject(_id, obj) )
    info_->generalBox->setTitle( tr("General object information for %1").arg( obj->name() ) );

  // ID
  info_->id->setText( locale.toString(_id) );
  // Vertices
  info_->vertices->setText( locale.toString( qulonglong(_mesh->n_vertices() ) ) );
  // Faces
  info_->faces->setText( locale.toString( qulonglong( _mesh->n_faces() ) ) );
  // Edges
  info_->edges->setText( locale.toString( qulonglong( _mesh->n_edges() ) ) );

  if ( face ) {

    // Picked Face
    info_->closestFaceLabel->setText( tr("Picked Face:") );
    info_->closestFaceLabel->show();
    info_->faceHandle->setText( locale.toString( _index ) );
    info_->faceHandle->show();

    // Closest Vertex
    info_->closestVertexLabel->setText( tr("Closest Vertex:") );
    info_->vertexHandle->setText( locale.toString( closestVertexIndex ) );

    // Closest Edge
    info_->closestEdgeLabel->setText( tr("Closest Edge:") );
    info_->edgeHandle->setText( locale.toString( closestEdgeIndex ) );
    info_->closestEdgeLabel->show();
    info_->edgeHandle->show();

    // Closest Edge Length
    info_->edgeLengthLabel->setText( tr("Closest Edge Length:") );
    info_->edgeLengthLabel->show();
    const typename MeshT::Point from = _mesh->point(OpenMesh::SmartEdgeHandle(closestEdgeIndex, _mesh).h0().from());
    const typename MeshT::Point to = _mesh->point(OpenMesh::SmartEdgeHandle(closestEdgeIndex, _mesh).h0().to());
    info_->edgeLength->setText( locale.toString( (to - from).norm() ) );
    info_->edgeLength->show();

    //adjacent vertex handles
    typename OpenMesh::SmartFaceHandle fh(_index,_mesh);

    QString adjacentVertices("");

    //adjacent vertex handles
    for ( auto fv_it : fh.vertices() ) {
        adjacentVertices += QString::number( fv_it.idx() )+";";
    }

    // Remove trailing ;
    adjacentVertices.chop(1);


    info_->adjVertexHandles->setText( adjacentVertices );
    info_->adjVertexHandles->show();
    info_->adjacentVertexLabel->show();

    //normal
    info_->normalLabel->setText(tr("Normal of picked face:"));
    info_->normalX->setText( QString::number( _mesh->normal(fh)[0],'f' ) );
    info_->normalY->setText( QString::number( _mesh->normal(fh)[1],'f' ) );
    info_->normalZ->setText( QString::number( _mesh->normal(fh)[2],'f' ) );
    info_->normalLabel->show();
    info_->normalLeft->show();
    info_->normalX->show();
    info_->normalY->show();
    info_->normalZ->show();
    info_->normalRight->show();

    // closest vertex coordinates
    info_->closestVertexPosLabel->setText(tr("Closest Vertex on the mesh:"));

  } else if (edge) {

    // Adjacent Faces
    info_->closestFaceLabel->setText( tr("Adjacent Faces:") );
    info_->closestFaceLabel->show();

    typename MeshT::HalfedgeHandle he1 = OpenMesh::SmartEdgeHandle(_index, _mesh).h0();
    typename MeshT::HalfedgeHandle he2 = OpenMesh::SmartEdgeHandle(_index, _mesh).h1();

    int fh1 = _mesh->face_handle(he1).idx();
    int fh2 = _mesh->face_handle(he2).idx();

    info_->faceHandle->setText( locale.toString( fh1 ) + ";" + locale.toString( fh2 ) );
    info_->faceHandle->show();

    // Adjacent vertices
    info_->adjVertexHandles->setText(QString::number( _mesh->from_vertex_handle(he1).idx() ) + ";" + QString::number( _mesh->to_vertex_handle(he1).idx() ));
    info_->adjVertexHandles->show();
    info_->adjacentVertexLabel->show();

    // Closest Vertex
    info_->closestVertexLabel->setText( tr("Closest Vertex:") );
    info_->vertexHandle->setText( locale.toString( closestVertexIndex ) );

    // Picked Edge
    info_->closestEdgeLabel->setText( tr("Picked Edge:") );
    info_->edgeHandle->setText( locale.toString( closestEdgeIndex ) );
    info_->closestEdgeLabel->show();
    info_->edgeHandle->show();

    // Edge Length
    info_->edgeLengthLabel->setText( tr("Edge Length:") );
    info_->edgeLengthLabel->show();

    const typename MeshT::Point from = _mesh->point(OpenMesh::SmartEdgeHandle(closestEdgeIndex, _mesh).h0().from());
    const typename MeshT::Point to = _mesh->point(OpenMesh::SmartEdgeHandle(closestEdgeIndex, _mesh).h0().to());

    info_->edgeLength->setText( locale.toString( (to - from).norm() ) );
    info_->edgeLength->show();

    // Normal
    info_->normalLabel->hide();
    info_->normalLeft->hide();
    info_->normalX->hide();
    info_->normalY->hide();
    info_->normalZ->hide();
    info_->normalRight->hide();

    // closest vertex coordinates
    info_->closestVertexPosLabel->setText(tr("Closest Vertex on the mesh:"));

  } else if (vertex) {

    // Faces
    info_->closestFaceLabel->hide();
    info_->faceHandle->hide();

    // Adjacent vertices
    info_->adjVertexHandles->hide();
    info_->adjacentVertexLabel->hide();

    // Closest Vertex
    info_->closestVertexLabel->setText( tr("Picked Vertex:") );
    info_->vertexHandle->setText( locale.toString( closestVertexIndex ) );

    // Closest Edge
    info_->closestEdgeLabel->hide();
    info_->edgeHandle->hide();

    // Edge Length
    info_->edgeLengthLabel->hide();
    info_->edgeLength->hide();

    // Normal
    OpenMesh::SmartVertexHandle vh(_index,_mesh);
    info_->normalLabel->setText(tr("Normal of picked vertex:"));
    info_->normalX->setText( QString::number( _mesh->normal(vh)[0],'f' ) );
    info_->normalY->setText( QString::number( _mesh->normal(vh)[1],'f' ) );
    info_->normalZ->setText( QString::number( _mesh->normal(vh)[2],'f' ) );
    info_->normalLabel->show();
    info_->normalLeft->show();
    info_->normalX->show();
    info_->normalY->show();
    info_->normalZ->show();
    info_->normalRight->show();

    // closest vertex coordinates
    info_->closestVertexPosLabel->setText(tr("Picked Vertex on the mesh:"));

    // Adjacent Edges
    info_->closestFaceLabel->setText( tr("Adjacent Edges:") );
    info_->closestFaceLabel->show();

    QString adjacentEdges("");

    //adjacent vertex handles
    for ( auto ve_it : vh.edges() ) {
        adjacentEdges += QString::number( ve_it.idx() )+";";
    }

    // Remove trailing ;
    adjacentEdges.chop(1);

    info_->faceHandle->setText( adjacentEdges );
    info_->faceHandle->show();
  }

  // closest vertex coordinates
  info_->vertexX->setText( QString::number( _mesh->point( _mesh->vertex_handle(closestVertexIndex) )[0],'f' ) );
  info_->vertexY->setText( QString::number( _mesh->point( _mesh->vertex_handle(closestVertexIndex) )[1],'f' ) );
  info_->vertexZ->setText( QString::number( _mesh->point( _mesh->vertex_handle(closestVertexIndex) )[2],'f' ) );


  // Components
  int compo_count = MeshInfo::componentCount(_mesh);
  info_->components->setText( locale.toString(compo_count));
  // Boundaries
  int boundary_count = MeshInfo::boundaryCount(_mesh);
  info_->boundaries->setText( locale.toString(boundary_count) );
  // Genus
  size_t chi = _mesh->n_vertices();
  chi -= _mesh->n_edges();
  chi += _mesh->n_faces(); // chi = Euler characteristic
  // chi + n_holes = 2(n_components - genus) => genus = n_components - (chi + n_holes)/2;
  double genus = compo_count - 0.5*(chi + boundary_count);
  if(compo_count == 1 && boundary_count == 0)
    info_->genus->setText( QString::number(genus) );
  else if(compo_count != 1)
    info_->genus->setText( "(multiple components)" );
  else
    info_->genus->setText( "(not manifold)" );

  // Coordinates

  auto maxX = -std::numeric_limits<double>::infinity();
  auto minX = std::numeric_limits<double>::infinity();
  //double sumX = 0.0;
  auto maxY = -std::numeric_limits<double>::infinity();
  auto minY = std::numeric_limits<double>::infinity();
  //double sumY = 0.0;
  auto maxZ = -std::numeric_limits<double>::infinity();
  auto minZ = std::numeric_limits<double>::infinity();
  //double sumZ = 0.0;
  auto minV = std::numeric_limits<int>::max();
  int maxV = 0;
  int sumV = 0;
  auto maxE = -std::numeric_limits<double>::infinity();
  auto minE = std::numeric_limits<double>::infinity();
  double sumE = 0.0;

  //iterate over all vertices
  for (auto v_it : _mesh->vertices()) {
    typename MeshT::Point p = _mesh->point( v_it );
    if (p[0] < minX) minX = p[0];
    if (p[0] > maxX) maxX = p[0];
    //sumX += p[0];
    if (p[1] < minY) minY = p[1];
    if (p[1] > maxY) maxY = p[1];
    //sumY += p[1];
    if (p[2] < minZ) minZ = p[2];
    if (p[2] > maxZ) maxZ = p[2];
    //sumZ += p[2];



    //check valence + edge length
    int valence = 0;

    for (auto vv_it : v_it.vertices()) {
      valence++;

      typename MeshT::Point p2 = _mesh->point( vv_it );
      typename MeshT::Scalar len = (p2 - p).norm();

      if (len < minE) minE = len;
      if (len > maxE) maxE = len;
      sumE += len;
    }

    if (valence < minV) minV = valence;
    if (valence > maxV) maxV = valence;
    sumV += valence;
  }

  //=============================
  // Vertex valence
  //=============================
  info_->valenceMin->setText( QString::number(minV) );
  info_->valenceMean->setText( QString::number( sumV / (double)_mesh->n_vertices(),'f' ) );
  info_->valenceMax->setText( QString::number(maxV) );

  //=============================
  // edge length
  //=============================
  if (_mesh->n_edges() >0 ) {
    info_->edgeMin->setText( QString::number(minE,'f') );
    info_->edgeMean->setText( QString::number( sumE / (_mesh->n_edges()*2),'f' )  );
    info_->edgeMax->setText( QString::number(maxE,'f') );
  } else {
    info_->edgeMin->setText( "-" );
    info_->edgeMean->setText( "-" );
    info_->edgeMax->setText( "-" );
  }


  //=============================
  // Triangle information
  //=============================
  if (_mesh->n_faces() > 0 ) {

    auto maxA = -std::numeric_limits<double>::infinity();
    auto minA = std::numeric_limits<double>::infinity();
    double sumA = 0.0;
    auto maxI = -std::numeric_limits<double>::infinity();
    auto minI = std::numeric_limits<double>::infinity();
    //double sumI = 0.0;
    auto maxD = -std::numeric_limits<double>::infinity();
    auto minD = std::numeric_limits<double>::infinity();
    double sumD = 0.0;
    int numD = 0;
    unsigned int maxFValence = std::numeric_limits<unsigned int>::min();
    unsigned int minFValence = std::numeric_limits<unsigned int>::max();
    size_t sumFValence = 0;

    //iterate over all faces
    for (auto f_it : _mesh->faces()) {
      typename MeshT::ConstFaceVertexIter cfv_it = _mesh->cfv_iter(f_it);

      const typename MeshT::Point v0 = _mesh->point( *cfv_it );
      ++cfv_it;
      const typename MeshT::Point v1 = _mesh->point( *cfv_it );
      ++cfv_it;
      const typename MeshT::Point v2 = _mesh->point( *cfv_it );

      const double aspect = ACG::Geometry::aspectRatio(v0, v1, v2);

      if (aspect < minA) minA = aspect;
      if (aspect > maxA) maxA = aspect;
      sumA += aspect;

      //inner triangle angles

      double angle = OpenMesh::rad_to_deg(acos(OpenMesh::sane_aarg( MathTools::sane_normalized(v2 - v0) | MathTools::sane_normalized(v1 - v0) )));

      if (angle < minI) minI = angle;
      if (angle > maxI) maxI = angle;
      //sumI += angle;

      angle = OpenMesh::rad_to_deg(acos(OpenMesh::sane_aarg( MathTools::sane_normalized(v2 - v1) | MathTools::sane_normalized(v0 - v1) )));

      if (angle < minI) minI = angle;
      if (angle > maxI) maxI = angle;
      //sumI += angle;

      angle = OpenMesh::rad_to_deg(acos(OpenMesh::sane_aarg( MathTools::sane_normalized(v1 - v2) | MathTools::sane_normalized(v0 - v2) )));

      if (angle < minI) minI = angle;
      if (angle > maxI) maxI = angle;
      //sumI += angle;

      //compute dihedral angles
      const typename MeshT::Normal n1 = _mesh->normal(f_it);

      for (auto ff_it : f_it.faces()) {

        const typename MeshT::Normal n2 = _mesh->normal(ff_it);

        angle = OpenMesh::rad_to_deg(acos(OpenMesh::sane_aarg( MathTools::sane_normalized(n1) | MathTools::sane_normalized(n2) )));

        if (angle < minD) minD = angle;
        if (angle > maxD) maxD = angle;
        sumD += angle;
        numD ++;
      }

      const unsigned int valence = _mesh->valence(f_it);
      minFValence = std::min(minFValence, valence);
      maxFValence = std::max(maxFValence, valence);
      sumFValence += valence;
    }

    info_->aspectMin->setText( QString::number(minA,'f') );
    info_->aspectMean->setText( QString::number( sumA / _mesh->n_faces(),'f' ) );
    info_->aspectMax->setText( QString::number(maxA,'f') );


    info_->angleMin->setText( QString::number(minI,'f') );
    info_->angleMean->setText( "-" );
    info_->angleMax->setText( QString::number(maxI,'f')  );

    info_->faceValenceMin->setText(tr("%1").arg(minFValence));
    info_->faceValenceMax->setText(tr("%1").arg(maxFValence));
    info_->faceValenceMean->setText(tr("%1").arg( static_cast<double>(sumFValence) / _mesh->n_faces()));

    if ( _mesh->n_faces() > 1 ) {
      info_->dihedralMin->setText( QString::number(minD,'f') );
      info_->dihedralMean->setText( QString::number( sumD / numD,'f' ) );
      info_->dihedralMax->setText( QString::number(maxD,'f') );
    } else {
      info_->dihedralMin->setText( "-" );
      info_->dihedralMean->setText( "-" );
      info_->dihedralMax->setText( "-" );
    }
  } else {

    // No triangles, no info
    info_->aspectMin->setText( "-" );
    info_->aspectMean->setText( "-" );
    info_->aspectMax->setText( "-" );


    info_->angleMin->setText( "-" );
    info_->angleMean->setText( "-" );
    info_->angleMax->setText( "-"  );

    info_->faceValenceMin->setText("-");
    info_->faceValenceMax->setText("-");
    info_->faceValenceMean->setText("-");

    info_->dihedralMin->setText( "-" );
    info_->dihedralMean->setText( "-" );
    info_->dihedralMax->setText( "-" );
  }


  //Calculate Bounding Box(min,max,cog)
  ACG::Vec3d min;
  ACG::Vec3d max;
  MeshInfo::getBoundingBox(_mesh, min, max);

  //Bounding Box Size
  ACG::Vec3d diff = max-min;

  info_->bbMinX->setText( QString::number(min[0],'f') );
  info_->bbMinY->setText( QString::number(min[1],'f') );
  info_->bbMinZ->setText( QString::number(min[2],'f') );

  info_->bbMaxX->setText( QString::number(max[0],'f') );
  info_->bbMaxY->setText( QString::number(max[1],'f') );
  info_->bbMaxZ->setText( QString::number(max[2],'f') );

  info_->bbSizeX->setText( QString::number(diff[0],'f') );
  info_->bbSizeY->setText( QString::number(diff[1],'f') );
  info_->bbSizeZ->setText( QString::number(diff[2],'f') );

  //COG
  ACG::Vec3d cog = MeshInfo::cog(_mesh);

  info_->cogX->setText( QString::number(cog[0],'f') );
  info_->cogY->setText( QString::number(cog[1],'f') );
  info_->cogZ->setText( QString::number(cog[2],'f') );

  //hitpoint
  info_->pointX->setText( QString::number( _hitPoint[0],'f' ) );
  info_->pointY->setText( QString::number( _hitPoint[1],'f' ) );
  info_->pointZ->setText( QString::number( _hitPoint[2],'f' ) );

  info_->setWindowFlags(info_->windowFlags() | Qt::WindowStaysOnTopHint);


  info_->show();
}

//----------------------------------------------------------------------------------------------

/** \brief Find closest vertex to selection
 *
 * @param _mesh     Reference to the mesh
 * @param _face_idx Index of the face that has been clicked on
 * @param _hitPoint The point that is used as the reference
 *
 * @return index of the closest vertex of the face to the hitpoint
 */

template <class MeshT>
int InfoMeshObjectPlugin::getClosestVertexInFace(MeshT* _mesh, int _face_idx, ACG::Vec3d& _hitPoint) {

    int closest_v_idx = 0;
    double dist = DBL_MAX;

    for (auto fv_it : OpenMesh::SmartFaceHandle(_face_idx, _mesh).vertices()) {


      // Find closest vertex to selection
      const typename MeshT::Point p = _mesh->point( fv_it );
      const ACG::Vec3d vTemp = ACG::Vec3d(p[0], p[1], p[2]);
      const double temp_dist = (vTemp - _hitPoint).length();

      if (temp_dist < dist) {
          dist = temp_dist;
          closest_v_idx = fv_it.idx();
      }

    }
    return closest_v_idx;
}

//-------------------------------------------------------------------------------------------

/** \brief Find closest edge to selection
 *
 * @param _mesh     Reference to the mesh
 * @param _face_idx Index of the face that has been clicked on
 * @param _hitPoint The point which will be tested
 *
 * @return index of the closest edge in the face to the hitpoint
 */

template <class MeshT>
int InfoMeshObjectPlugin::getClosestEdgeInFace(MeshT* _mesh, int _face_idx, const ACG::Vec3d& _hitPoint) {

    typename MeshT::VertexHandle v1, v2;
    typename MeshT::Point p1, p2;

    ACG::Vec3d vp1, vp2, h;
    double dist = DBL_MAX;
    int closest_e_handle = 0;

    for (auto fh_it : OpenMesh::SmartFaceHandle(_face_idx, _mesh).halfedges()) {

      v1 = fh_it.from();
      v2 = fh_it.to();

      p1 = _mesh->point(v1);
      p2 = _mesh->point(v2);

      vp1 = ACG::Vec3d(p1[0], p1[1], p1[2]);
      vp2 = ACG::Vec3d(p2[0], p2[1], p2[2]);

      const ACG::Vec3d e = (vp2 - vp1).normalized();
      const ACG::Vec3d g = _hitPoint - vp1;
      const double x = g | e;

      const double temp_dist = (_hitPoint - (vp1 + x * e)).length();

      if (temp_dist < dist) {
          closest_e_handle = fh_it.edge().idx();
          dist = temp_dist;
      }
    }

    return closest_e_handle;
}

//----------------------------------------------------------------------------------------------

/** \brief Find closest vertex on the edge (endpoint)
 *
 * @param _mesh     Reference to the mesh
 * @param _edge_idx Index of the edge that has been clicked on
 * @param _hitPoint The point which will be tested
 *
 * @return index of the closest vertex on the edge
 */

template <class MeshT>
int InfoMeshObjectPlugin::getClosestVertexFromEdge(MeshT* _mesh, int _edge_idx, ACG::Vec3d& _hitPoint) {

    ACG::Vec3d toVertex = _mesh->point(OpenMesh::SmartEdgeHandle(_edge_idx, _mesh).h0().to());
    ACG::Vec3d fromVertex = _mesh->point(OpenMesh::SmartEdgeHandle(_edge_idx, _mesh).h0().from());

    double distTo   = (_hitPoint - toVertex  ).norm();
    double distFrom = (_hitPoint - fromVertex).norm();

    if ( distTo > distFrom )
      return OpenMesh::SmartEdgeHandle(_edge_idx, _mesh).h0().from().idx();
    else
      return OpenMesh::SmartEdgeHandle(_edge_idx, _mesh).h0().to().idx();

}

//----------------------------------------------------------------------------------------------

void
InfoMeshObjectPlugin::
  slotInformationRequested(const QPoint _clickedPoint, DataType _type) {

    // Only respond on mesh objects
    if((_type != DATA_TRIANGLE_MESH) && (_type != DATA_POLY_MESH)) return;

    ACG::SceneGraph::PickTarget target = ACG::SceneGraph::PICK_FACE;

    size_t         node_idx, target_idx;
    ACG::Vec3d     hit_point;

    if (info_->isHidden())
    {
      //user couldn't select the pick mode,
      //so we have to do this
      target = ACG::SceneGraph::PICK_ANYTHING;
      if (!PluginFunctions::scenegraphPick(target, _clickedPoint, node_idx, target_idx, &hit_point))
        return;

      BaseObjectData* object;
      if (!PluginFunctions::getPickedObject(node_idx, object) )
        return;

      //object is picked, now we can decide, what the user wants to pick
      //priority: face > edge > vertex
      if ( object->dataType(DATA_TRIANGLE_MESH) )
      {
        TriMesh* mesh = PluginFunctions::triMesh(object);
        if (mesh->n_faces() != 0)
          info_->pickMode->setCurrentIndex(0);
        else if (mesh->n_edges() != 0)
          info_->pickMode->setCurrentIndex(1);
        else
          info_->pickMode->setCurrentIndex(2);
      }
      else if ( object->dataType(DATA_POLY_MESH) )
      {
        PolyMesh* mesh = PluginFunctions::polyMesh(object);
        if (mesh->n_faces() != 0)
          info_->pickMode->setCurrentIndex(0);
        else if (mesh->n_edges() != 0)
          info_->pickMode->setCurrentIndex(1);
        else
          info_->pickMode->setCurrentIndex(2);
      }
    }

    if (info_->pickMode->currentIndex() == 1 )
      target = ACG::SceneGraph::PICK_EDGE;
    else if (info_->pickMode->currentIndex() == 2 )
      target = ACG::SceneGraph::PICK_VERTEX;
    else
      target = ACG::SceneGraph::PICK_FACE;

    if (PluginFunctions::scenegraphPick(target, _clickedPoint, node_idx, target_idx, &hit_point)) {
      BaseObjectData* object;

      if ( PluginFunctions::getPickedObject(node_idx, object) ) {

         emit log( LOGINFO , object->getObjectinfo() );

         lastPickedObject_ = object;
         lastPickedObjectId_ = object->id();

         if ( object->dataType(DATA_TRIANGLE_MESH) )
           printMeshInfo( PluginFunctions::triMesh(object) , object->id(), target_idx, hit_point );

         if ( object->dataType(DATA_POLY_MESH) )
           printMeshInfo( PluginFunctions::polyMesh(object) , object->id(), target_idx, hit_point );
      } else {
          lastPickedObject_ = 0;
          return;
      }
    }
    else
    {
      emit log( LOGERR , tr("Unable to pick object.") );
    }
}

//------------------------------------------------------------------------------

template< class MeshT >
void InfoMeshObjectPlugin::getEdgeLengths(MeshT* _mesh, double &min, double &max, double &mean)
{

  min = std::numeric_limits<double>::infinity();
  max = -std::numeric_limits<double>::infinity();
  mean = 0.0;

  for (auto e_it : _mesh->edges())
  {
    typename MeshT::Scalar len = (_mesh->point(e_it.h0().to()) -
                                    _mesh->point(e_it.h1().to())).norm ();
    if (len < min) min = len;
    if (len > max) max = len;
    mean += len;
  }

  mean /= _mesh->n_edges();
}

//------------------------------------------------------------------------------

bool InfoMeshObjectPlugin::getEdgeLengths(int _id, double &min, double &max, double &mean)
{
  BaseObjectData* object;
  if ( ! PluginFunctions::getObject(_id,object) )
    return false;

  if ( object == 0){
    emit log(LOGERR, tr("Unable to get object"));
    return false;
  }

  if ( object->dataType(DATA_TRIANGLE_MESH) ) {
    TriMesh* mesh = PluginFunctions::triMesh(object);

    if ( mesh == 0 ) {
      emit log(LOGERR,tr("Unable to get mesh"));
      return false;
    }

    getEdgeLengths (mesh, min, max, mean);
    return true;

  } else {
    PolyMesh* mesh = PluginFunctions::polyMesh(object);

    if ( mesh == 0 ) {
      emit log(LOGERR,tr("Unable to get mesh"));
      return false;
    }

    getEdgeLengths (mesh, min, max, mean);
    return true;
  }

  return false;
}

//------------------------------------------------------------------------------

void InfoMeshObjectPlugin::updateData( int _identifier , const UpdateType& _type, const bool _deleted){

  if ( !infoBar_ ) {
     return;
   }

  BaseObjectData* object;
  PluginFunctions::getObject(_identifier,object);

  if (_identifier == lastPickedObjectId_ && _deleted) {
      lastPickedObject_ = 0;
      lastPickedObjectId_ = -1;
  }

  // Last object that is target has been removed.
  if ( _deleted && object && object->target() && (PluginFunctions::targetCount() == 1) ) {
    infoBar_->hideCounts();
    return;
  }

   // We only show the information in the status bar if one target mesh is selected or
   // If 2 targets where selected, where one is deleted which was target
   if ( PluginFunctions::targetCount() == 1 || ( _deleted && (PluginFunctions::targetCount() == 2) && object && object->target() ) ) {



     // The object that caused the update is not a target anymore.
     // Therefore we need to get the remaining target by iteration.
     // If something was deleted, we might see this object here, so make sure, to not take the one with the same id as the deleted one
     if ( object && !object->target() ) {
         for (auto* o_it : PluginFunctions::objects(PluginFunctions::TARGET_OBJECTS) ) {
         if ( !_deleted || ( o_it->id() != _identifier ) ) {
           object = o_it;
           break;
         }
       }
     }

     // We only need to update something, if the updated object is the target object
     if (object && object->target() ) {

       if (object->dataType(DATA_TRIANGLE_MESH)){

         TriMesh* mesh = PluginFunctions::triMesh(object);

         infoBar_->vertices->setText( QLocale::system().toString( qulonglong(mesh->n_vertices()) ) );
         infoBar_->edges->setText( QLocale::system().toString( qulonglong(mesh->n_edges()) ) );
         infoBar_->faces->setText( QLocale::system().toString( qulonglong(mesh->n_faces()) ) );

         infoBar_->showCounts();

         return;
       }

       if (object->dataType(DATA_POLY_MESH)){

         PolyMesh* mesh = PluginFunctions::polyMesh(object);

         infoBar_->vertices->setText( QLocale::system().toString( qulonglong(mesh->n_vertices()) ) );
         infoBar_->edges->setText( QLocale::system().toString( qulonglong(mesh->n_edges()) ) );
         infoBar_->faces->setText( QLocale::system().toString( qulonglong(mesh->n_faces()) ) );

         infoBar_->showCounts();
         return;
       }

     }

     infoBar_->hideCounts();

   } else {
     // Display only count information
     if ( (PluginFunctions::targetCount() > 1) && object ) {
       if ( _deleted && object->target() ) {
         infoBar_->showTargetCount( PluginFunctions::targetCount() - 1);
       } else {
         infoBar_->showTargetCount( PluginFunctions::targetCount() );
       }
     } else
       infoBar_->hideCounts();
   }

}

//------------------------------------------------------------------------------

void InfoMeshObjectPlugin::slotObjectUpdated( int _identifier , const UpdateType& _type){

  updateData(_identifier,_type,false);

}

//------------------------------------------------------------------------------

void InfoMeshObjectPlugin::slotObjectSelectionChanged( int _identifier ){
  updateData(_identifier,UPDATE_ALL,false);
}

//------------------------------------------------------------------------------

void InfoMeshObjectPlugin::objectDeleted( int _identifier ){
  updateData(_identifier,UPDATE_ALL,true);
}


//------------------------------------------------------------------------------

void InfoMeshObjectPlugin::slotAllCleared(){
  if ( infoBar_ )
    infoBar_->hideCounts();

}

void InfoMeshObjectPlugin::slotShowHistogram() {
    if (!lastPickedObject_) return;

    ValenceHistogramDialog *dialog = 0;
    {
        TriMeshObject *tmo = dynamic_cast<TriMeshObject*>(lastPickedObject_);
        if (tmo) {
            dialog = new ValenceHistogramDialog(*tmo->mesh(), info_);
        }
    }

    if (!dialog) {
        PolyMeshObject *pmo = dynamic_cast<PolyMeshObject*>(lastPickedObject_);
        if (pmo) {
            dialog = new ValenceHistogramDialog(*pmo->mesh(), info_);
        }
    }

    dialog->setAttribute(Qt::WA_DeleteOnClose, true);
    dialog->show();
    dialog->raise();
}



